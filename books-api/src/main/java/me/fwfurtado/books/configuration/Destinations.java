package me.fwfurtado.books.configuration;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface Destinations {

    String OTHER = "other";
    String INPUT = "input";
    String BOOK = "book";

    @Input(OTHER)
    SubscribableChannel other();

    @Input(INPUT)
    SubscribableChannel input();

    @Output(BOOK)
    MessageChannel books();
}
