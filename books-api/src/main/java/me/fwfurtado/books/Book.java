package me.fwfurtado.books;

public class Book {

    private Long id;
    private String title;
    private long authorId;

    public Book(Long id, String title, long authorId) {
        this.id = id;
        this.title = title;
        this.authorId = authorId;
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public Long getAuthorId() {
        return authorId;
    }
}
