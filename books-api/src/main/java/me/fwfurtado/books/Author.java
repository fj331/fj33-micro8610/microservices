package me.fwfurtado.books;

import java.util.StringJoiner;

public class Author {

    private Long id;
    private String name;

    /**
     * @deprecated frameworks only
     */
    @Deprecated
    Author() {
    }

    public Author(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Author.class.getSimpleName() + "[", "]")
            .add("id=" + id)
            .add("name='" + name + "'")
            .toString();
    }
}
