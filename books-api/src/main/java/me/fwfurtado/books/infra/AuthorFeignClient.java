package me.fwfurtado.books.infra;

import me.fwfurtado.books.Author;
import me.fwfurtado.books.infra.AuthorFeignClient.AuthorFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "authors", fallback = AuthorFallback.class)
public interface AuthorFeignClient {

    @GetMapping("authors/{id}")
    Author findById(@PathVariable("id") Long id);

    @Component
    class AuthorFallback implements AuthorFeignClient {

        @Override
        public Author findById(Long id) {
            return new Author(id, "Unknown");
        }
    }
}
