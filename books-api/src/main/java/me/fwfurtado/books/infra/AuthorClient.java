package me.fwfurtado.books.infra;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import me.fwfurtado.books.Author;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AuthorClient {

    private final RestTemplate rest;
    private final String authorsURL;

    public AuthorClient(RestTemplate rest, @Value("${services.author.url}") String authorsURL) {
        this.rest = rest;
        this.authorsURL = authorsURL;
    }

    @HystrixCommand(fallbackMethod = "findByIdFallback")
    public Author findById(Long authorId) {
        return rest.getForObject(authorsURL, Author.class, authorId);
    }

    public Author findByIdFallback(Long authorId) {
        return new Author(authorId, "Unknown");
    }
}
