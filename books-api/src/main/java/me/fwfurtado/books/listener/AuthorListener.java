package me.fwfurtado.books.listener;

import me.fwfurtado.books.Author;
import me.fwfurtado.books.configuration.Destinations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;

@Component
class AuthorListener {

    private final static Logger LOG = LoggerFactory.getLogger(AuthorListener.class);

    @StreamListener(Destinations.INPUT)
    void handle(Author author) {
        LOG.info("Received a new author {}", author);
    }

}
