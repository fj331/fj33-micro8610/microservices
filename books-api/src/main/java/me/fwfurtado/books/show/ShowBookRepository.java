package me.fwfurtado.books.show;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import me.fwfurtado.books.Author;
import me.fwfurtado.books.Book;
import me.fwfurtado.books.infra.AuthorClient;
import me.fwfurtado.books.infra.AuthorFeignClient;
import org.springframework.stereotype.Repository;

@Repository
class ShowBookRepository {

    private final static Map<Long, Book> database = new HashMap<>();

    static {
        database.computeIfAbsent(1L, id -> new Book(id, "Spring Framework", 1L));
        database.computeIfAbsent(2L, id -> new Book(id, "Spring MVC", 1L));
        database.computeIfAbsent(3L, id -> new Book(id, "Spring Cloud", 2L));
    }

    private final AuthorClient authors;
    private final AuthorFeignClient feignClient;

    ShowBookRepository(AuthorClient authors, AuthorFeignClient feignClient) {
        this.authors = authors;
        this.feignClient = feignClient;
    }

    Optional<BookView> findById(Long id) {
        return Optional.ofNullable(database.get(id)).map(this::fetchAuthor);
    }

    private BookView fetchAuthor(Book book) {

        Author author = feignClient.findById(book.getAuthorId());
//        Author author = authors.findById(book.getAuthorId());

        return new BookView(book.getId(), book.getTitle(), author.getName());
    }

}
