package me.fwfurtado.books.show;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

import me.fwfurtado.books.Book;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("books")
class ShowBookController {

    private final long port;

    private final ShowBookRepository repository;

    ShowBookController(@Value("${server.port}") long port, ShowBookRepository repository) {
        this.port = port;
        this.repository = repository;
    }

    @GetMapping("{id}")
    ResponseEntity<BookView> show(@PathVariable Long id) {
        return repository.findById(id).map(ok()::body).orElseGet(notFound()::build);
    }

    @Retryable(maxAttempts = 6, backoff = @Backoff(multiplier = 2.0, delay = 200L, maxDelay = 1200))
    @GetMapping("{id}/retry")
    ResponseEntity<?> retry(@PathVariable Long id) {
        if (id == 2)  {
            throw new RuntimeException("Deu Ruim");
        }

        return ok().build();
    }

}
