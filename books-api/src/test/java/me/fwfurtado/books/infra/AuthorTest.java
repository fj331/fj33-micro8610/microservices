package me.fwfurtado.books.infra;

import static org.junit.Assert.assertEquals;

import me.fwfurtado.books.Author;
import me.fwfurtado.books.infra.AuthorClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = "me.fwfurtado:authors-api:+:stubs:9090")
public class AuthorTest {

    private RestTemplate client = new RestTemplate();

    static {
        System.setProperty("eureka.client.enabled", "false");
        System.setProperty("spring.cloud.config.failFast", "false");
    }

    @Test
    public void test() {
        Author author = client.getForObject("http://localhost:9090/authors/{id}",Author.class, 1L);

        assertEquals(Long.valueOf(1L), author.getId());
        assertEquals("Fernando", author.getName());
    }

}
