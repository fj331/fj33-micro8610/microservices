package me.fwfurtado.books.listener;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.StubTrigger;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(ids = "me.fwfurtado:authors-api:+:stubs:9090")
public class AuthorListenerTest {

    @Autowired
    private StubTrigger trigger;


    @Test
    public void test() {
        trigger.trigger("notify-authors-creation");
    }

}
