package me.fwfurtado.authors.show;

import static org.mockito.Mockito.when;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import java.util.Optional;
import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.AuthorRepository;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.contract.verifier.messaging.boot.AutoConfigureMessageVerifier;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMessageVerifier
public abstract class ShowBaseTest {

    @MockBean
    private AuthorRepository repository;

    @Autowired
    @Output(Source.OUTPUT)
    private MessageChannel channel;

    private Author fernando = new Author(1L, "Fernando");
    private Author alberto = new Author(1L, "Alberto");

    public void fireEvent() {

        Message<Author> message = MessageBuilder.withPayload(alberto).build();

        channel.send(message);
    }

    @Before
    public void setUp() throws Exception {


        when(repository.findById(1L)).thenReturn(Optional.of(fernando));

        RestAssuredMockMvc.standaloneSetup(new ShowController(repository));
    }
}
