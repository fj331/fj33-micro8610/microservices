import org.springframework.cloud.contract.spec.Contract

Contract.make {

    label 'notify-authors-creation'

    input {
        triggeredBy('fireEvent()')
    }

    outputMessage {
        sentTo('authors')

        body([
                "id": 1,
                "name": "Alberto"
        ])

    }
}