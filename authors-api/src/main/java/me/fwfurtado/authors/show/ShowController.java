package me.fwfurtado.authors.show;

import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.AuthorRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("authors")
public class ShowController {

    private final AuthorRepository repository;

    public ShowController(AuthorRepository repository) {
        this.repository = repository;
    }

    @GetMapping("{id}")
    ResponseEntity<Author> show(@PathVariable Long id) {

        return repository.findById(id).map(ok()::body).orElseGet(notFound()::build);
    }

}
