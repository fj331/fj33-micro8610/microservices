package me.fwfurtado.authors;

import static java.util.Objects.isNull;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorRepository {

    private static final AtomicLong identity = new AtomicLong();
    private static final Map<Long, Author> database = new HashMap<>();

    static {

        database.computeIfAbsent(identity.incrementAndGet(), id -> new Author(id, "Fernando"));
    }

    public Optional<Author> findById(Long id) {
        return Optional.ofNullable(database.get(id));
    }

    public void save(Author author) {
        if (isNull(author.getId())) {
            author.setId(identity.incrementAndGet());
        }

        database.put(author.getId(), author);
    }
}
