package me.fwfurtado.authors.register;

import me.fwfurtado.authors.Author;
import me.fwfurtado.authors.AuthorRepository;
import me.fwfurtado.authors.register.RegisterController.AuthorForm;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class RegisterService {

    private final AuthorRepository repository;
    private final MessageChannel channel;

    public RegisterService(AuthorRepository repository, @Output(Source.OUTPUT) MessageChannel channel) {

        this.repository = repository;
        this.channel = channel;
    }

    Long createBy(AuthorForm form) {
        Author author = new Author(form.getName());

        repository.save(author);

        Message<Author> message = MessageBuilder.withPayload(author).build();

        channel.send(message);

        return author.getId();
    }
}
