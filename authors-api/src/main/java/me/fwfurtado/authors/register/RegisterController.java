package me.fwfurtado.authors.register;

import static org.springframework.http.ResponseEntity.created;

import java.net.URI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("authors")
class RegisterController {

    private final RegisterService service;

    RegisterController(RegisterService service) {
        this.service = service;
    }

    @PostMapping
    ResponseEntity<?> create(@RequestBody AuthorForm form, UriComponentsBuilder uriBuilder) {

        Long id = service.createBy(form);

        URI uri = uriBuilder.path("{id}").build(id);

        return created(uri).build();

    }

    static class AuthorForm {

        private String name;

        /**
         * @deprecated frameworks only
         */
        @Deprecated
        AuthorForm() {
        }

        public AuthorForm(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }
}
